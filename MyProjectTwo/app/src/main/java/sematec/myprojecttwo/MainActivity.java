package sematec.myprojecttwo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    View Username;
    View Password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Username = findViewById(R.id.showUsername);
        Password = findViewById(R.id.showPassword);

        findViewById(R.id.Start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent destinationIntent = new Intent(MainActivity.this, SecondActivity.class);
                startActivityForResult(destinationIntent, 100);

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                String usernameValue = data.getStringExtra("Username");
                String passwordValue = data.getStringExtra("Password");
                Username.setText(usernameValue);
                Password.setText(passwordValue);
            } else {
                Toast.makeText(this, "Data Faild", Toast.LENGTH_SHORT).show();
            }
        }



    }
}
